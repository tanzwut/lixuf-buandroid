package mx.lixuf.buapp

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.gson.responseObject
import mx.lixuf.buapp.databinding.ScrollviewBinding
import mx.lixuf.buapp.model.Card
import mx.lixuf.buapp.util.API
import mx.lixuf.buapp.util.SwipeToDeleteCallback
import org.json.JSONObject

class MainActivity: AppCompatActivity() {

    private lateinit var binding: ScrollviewBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ScrollviewBinding.inflate(layoutInflater)
        setContentView(binding.root)

        updateWallet()

        handleIntent(intent)

        binding.search.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String?): Boolean {
                val cardAdapter = binding.lista.adapter as CardAdapter
                cardAdapter.filter.filter(newText)
                return true
            }

            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }
        })
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        if (intent != null)
            handleIntent(intent)
    }

    private fun updateWallet() {
        val preferences = getSharedPreferences(
            resources.getString(R.string.app_name),
            Context.MODE_PRIVATE
        )
        val accessToken = preferences.getString("accessToken", "null")

        val headers = HashMap<String, Any>()
        headers["Content-Type"] = "application/json"
        headers["Authorization"] = "Bearer $accessToken"

        Fuel.get(API.getWalletUrl())
            .header(headers)
            .responseObject<Collection<Card>> { _, _, result ->
                val (cards, error) = result

                if (cards != null){
                    setAdapter(cards = cards.toMutableList())
                }
                else {
                    Toast.makeText(applicationContext, error?.localizedMessage, Toast.LENGTH_LONG).show()
                }
            }
    }

    private fun setAdapter(cards: MutableList<Card>) {
        val adapter = CardAdapter(cards)
        binding.lista.layoutManager = LinearLayoutManager(this)
        binding.lista.adapter = adapter

        val swipeHandler = object : SwipeToDeleteCallback(this) {

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val cardAdapter = binding.lista.adapter as CardAdapter

                val preferences = getSharedPreferences(
                    resources.getString(R.string.app_name),
                    Context.MODE_PRIVATE
                )
                val accessToken = preferences.getString("accessToken", "null")

                val headers = HashMap<String, Any>()
                headers["Content-Type"] = "application/json"
                headers["Authorization"] = "Bearer $accessToken"

                val parameters = listOf("ID_tarjeta" to cardAdapter.getItemId(viewHolder.adapterPosition))

                Fuel.post(API.getRemoveCardUrl(), parameters)
                        .header(headers)
                        .responseString { _, _, result ->
                            val (stringResult, error) = result

                            if (stringResult != null) {
                                val json = JSONObject(stringResult)

                                if (json.has("status")) {
                                    Toast.makeText(
                                        this@MainActivity,
                                        json.getString("status"),
                                        Toast.LENGTH_LONG
                                    ).show()
                                    cardAdapter.removeAt(viewHolder.adapterPosition)
                                }
                                else {
                                    Toast.makeText(
                                        this@MainActivity,
                                        "Ocurrió un error al eliminar la tarjeta",
                                        Toast.LENGTH_LONG
                                    ).show()
                                }
                            }
                            else {
                                Toast.makeText(
                                    this@MainActivity,
                                    error?.localizedMessage,
                                    Toast.LENGTH_LONG
                                ).show()
                            }
                        }
            }
        }
        val itemTouchHelper = ItemTouchHelper(swipeHandler)
        itemTouchHelper.attachToRecyclerView(binding.lista)
    }

    private fun handleIntent(intent: Intent) {
        val appLinkAction = intent.action
        val appLinkData: Uri? = intent.data

        if (appLinkAction == Intent.ACTION_VIEW) {
            appLinkData?.lastPathSegment?.also { cardId ->
                Uri.parse("content://mx.lixuf.BUApp/share/")
                        .buildUpon()
                        .appendPath(cardId)
                        .build().also { appData ->
                            showCard(appData)
                        }
            }
        }
    }

    private fun showCard(cardUri: Uri) {
        val intent = Intent(this@MainActivity, AddCardActivity::class.java)
        intent.apply {
            putExtra(Intent.EXTRA_TEXT, cardUri.lastPathSegment)
        }

        startActivityForResult(intent, ADD_CARD_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (resultCode) {
            AddCardActivity.ADD_RESULT_CODE_OK -> {

                updateWallet()

                val resultText = data?.extras?.getString(Intent.EXTRA_RETURN_RESULT) ?: return
                Toast.makeText(applicationContext, resultText, Toast.LENGTH_LONG).show()
            }
            AddCardActivity.ADD_RESULT_CODE_CANCELED -> {
                val resultText = data?.extras?.getString(Intent.EXTRA_RETURN_RESULT) ?: return

                Toast.makeText(applicationContext, resultText, Toast.LENGTH_LONG).show()
            }
            AddCardActivity.ADD_RESULT_CODE_ERROR -> {
                val resultText = data?.extras?.getString(Intent.EXTRA_RETURN_RESULT) ?: return

                Toast.makeText(applicationContext, resultText, Toast.LENGTH_LONG).show()
            }
        }
    }


    companion object {
        const val ADD_CARD_REQUEST_CODE = 9000
    }
}