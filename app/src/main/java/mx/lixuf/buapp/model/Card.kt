package mx.lixuf.buapp.model

import com.google.gson.annotations.SerializedName

data class Card(val id: String, val nombre: String, val whatsapp: String? = null, val email: String, @SerializedName("puesto") val profession: String? = null, @SerializedName("contacto") val phone: String? = null, val image: String? = null, val direccion: String? = null)