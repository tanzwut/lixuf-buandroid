package mx.lixuf.buapp

import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidmads.library.qrgenearator.QRGContents
import androidmads.library.qrgenearator.QRGEncoder
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.gms.common.util.Base64Utils
import mx.lixuf.buapp.databinding.MaincardviewBinding
import mx.lixuf.buapp.model.Card

class CardAdapter(private var cards: MutableList<Card>): RecyclerView.Adapter<CardAdapter.CardHolder>(), Filterable {

    val originalList: MutableList<Card> = cards

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CardHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.maincardview, parent, false)

        return CardHolder(view)
    }

    fun removeAt(position: Int) {
        cards.removeAt(position)
        notifyItemRemoved(position)
    }

    override fun onBindViewHolder(holder: CardHolder, position: Int) {
        holder.render(cards[position])
    }

    override fun getItemCount(): Int = cards.size

    override fun getItemId(position: Int): Long = cards[position].id.toLong()

    class CardHolder(view: View): RecyclerView.ViewHolder(view) {
        private val binding = MaincardviewBinding.bind(view)

        fun render(card: Card) {
            binding.txtName.text = card.nombre
            binding.txtProfession.text = card.profession ?: ""

            if (card.whatsapp != null) {
                binding.btnWhatsapp.setOnClickListener {
                    val intent = Intent(Intent.ACTION_SEND).apply {
                        type = "text/plain"
                        putExtra(Intent.EXTRA_PHONE_NUMBER, card.whatsapp)
                        `package` = "com.whatsapp"
                    }

                    if (intent.resolveActivity(it.context.packageManager) != null) {
                        it.context.startActivity(intent)
                    }
                }
            }
            else {
                binding.btnWhatsapp.visibility = View.GONE
            }

            binding.btnMail.setOnClickListener {
                val addresses = arrayListOf(card.email)
                val intent = Intent(Intent.ACTION_SENDTO).apply {
                    data = Uri.parse("mailto:")
                    putExtra(Intent.EXTRA_EMAIL, addresses.toTypedArray())
                }

                if (intent.resolveActivity(it.context.packageManager) != null) {
                    it.context.startActivity(intent)
                }
            }

            if (card.phone != null) {
                binding.btnPhoneCall.setOnClickListener {
                    val intent = Intent(Intent.ACTION_DIAL).apply {
                        data = Uri.parse("tel:${card.phone}")
                    }

                    if (intent.resolveActivity(it.context.packageManager) != null) {
                        it.context.startActivity(intent)
                    }
                }
            }
            else {
                binding.btnPhoneCall.visibility = View.GONE
            }

            binding.btnShare.setOnClickListener {
                val intent = Intent(Intent.ACTION_SEND).apply {
                    putExtra(Intent.EXTRA_TEXT, "https://solyfel.com/share/${card.id}")
                    type = "text/*"
                }

                it.context.startActivity(Intent.createChooser(intent, null))
            }

            if (card.direccion != null) {
                binding.btnMap.setOnClickListener {
                    val intent = Intent(Intent.ACTION_VIEW).apply {
                        data = Uri.parse("geo:0,0?q=${card.direccion}")
                    }

                    if (intent.resolveActivity(it.context.packageManager) != null) {
                        it.context.startActivity(intent)
                    }
                }
            }
            else {
                binding.btnMap.visibility = View.GONE
            }

            val qrGen = QRGEncoder("https://solyfel.com/share/${card.id}", null, QRGContents.Type.TEXT, 100)
            qrGen.colorBlack = Color.WHITE
            qrGen.colorWhite = Color.TRANSPARENT

            val bitmap = qrGen.bitmap

            Glide.with(itemView)
                    .load(bitmap)
                    .into(binding.qrCode)

            val imgArray = Base64Utils.decode(card.image ?: return)

            Glide.with(itemView)
                .load(imgArray)
                .centerCrop()
                .placeholder(R.drawable.image2)
                .into(binding.profileImage)

        }
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                if (results != null) {
                    cards = results.values as MutableList<Card>
                    notifyDataSetChanged()
                }
            }

            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val filteredCards: MutableList<Card>

                if (constraint!!.isEmpty()) {
                    filteredCards = originalList
                }
                else {
                    filteredCards = getFilteredResults(constraint.toString())
                }

                val filterResults = FilterResults()
                filterResults.values = filteredCards

                return filterResults
            }
        }
    }

    private fun getFilteredResults(constraint: String): MutableList<Card> {
        val results: MutableList<Card> = ArrayList()

        for (card in originalList) {
            if (card.nombre.contains(constraint, true)) {
                results.add(card)
            }
        }

        return results
    }
}