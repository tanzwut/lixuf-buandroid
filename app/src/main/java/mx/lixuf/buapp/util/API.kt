package mx.lixuf.buapp.util

class API {
    companion object {
        //private const val base_url = "https://solyfel.com/"
        private const val base_url = "http://solyfel.lixuf.com/"
        private const val login_endpoint = "api/login"
        private const val wallet_endpoint = "api/wallet"
        private const val cards_endpoint = "api/card"
        private const val add_card_endpoint = "api/addCard"
        private const val remove_card_endpoint = "api/removeCard"

        fun getLoginUrl() : String = base_url + login_endpoint
        fun getWalletUrl(): String = base_url + wallet_endpoint
        fun  getAddCardUrl(): String = base_url + add_card_endpoint
        fun  getRemoveCardUrl(): String = base_url + remove_card_endpoint
        fun getCardsUrl(): String = base_url + cards_endpoint
        fun getCardUrl(cardId: String) = "$base_url$cards_endpoint/$cardId"
    }
}