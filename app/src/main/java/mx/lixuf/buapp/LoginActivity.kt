package mx.lixuf.buapp

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.github.kittinunf.fuel.Fuel
import mx.lixuf.buapp.databinding.ActivityMainBinding
import mx.lixuf.buapp.util.API
import org.json.JSONObject


class LoginActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)

        binding.textFieldUser.editText?.setText("api@apibeyou.com")
        binding.textFieldPassword.editText?.setText("apibeyouapi")

        binding.button.setOnClickListener {
            val user = binding.textFieldUser.editText?.text.toString()
            val password = binding.textFieldPassword.editText?.text.toString()

            val parameters = listOf("email" to user, "password" to password)

            Fuel.post(API.getLoginUrl(), parameters)
                    .responseString { _, _, result ->
                        val (stringResult, error) = result
                        if (stringResult != null) {
                            val json = JSONObject(stringResult)

                            if (json.has("access_token")) {
                                val preferences = getSharedPreferences(resources.getString(R.string.app_name), Context.MODE_PRIVATE)
                                with(preferences.edit()) {
                                    putString("accessToken", json.getString("access_token"))
                                    apply()

                                    val intent = Intent(this@LoginActivity, MainActivity::class.java)

                                    startActivity(intent)
                                }
                            }
                            else {
                                Toast.makeText(this, "Usuario/Contraseña incorrecto", Toast.LENGTH_LONG).show()
                            }
                        }
                        else {
                            Toast.makeText(this, error?.localizedMessage, Toast.LENGTH_LONG).show()
                        }
                    }
        }

        setContentView(binding.root)

        /*val adapter = CardAdapter(cards)
        binding.lista.layoutManager = LinearLayoutManager(this)
        binding.lista.adapter = adapter

        binding.imageButton.setOnClickListener {
            if (isApplicationInstalled("com.whatsapp", packageManager)) {
                val sendIntent = Intent()
                sendIntent.action = ACTION_SEND
                sendIntent.putExtra(Intent.EXTRA_TEXT, "This is my text to send.")
                sendIntent.type = "text/plain"
                sendIntent.setPackage("com.whatsapp")

                startActivity(sendIntent)
            }
            else {
                try {
                    val playStoreIntent = Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.whatsapp"))

                    startActivity(playStoreIntent)
                }
                catch (error: ActivityNotFoundException) {
                    val playStoreIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id==com.whatsapp"))

                    startActivity(playStoreIntent)
                }
            }
        }*/
    }

    private fun isApplicationInstalled(packageName: String, packageManager: PackageManager): Boolean {
        return try {
            packageManager.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES)
            true
        }
        catch (error: PackageManager.NameNotFoundException) {
            false
        }
    }

}