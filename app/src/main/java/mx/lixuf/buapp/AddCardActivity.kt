package mx.lixuf.buapp

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.gson.responseObject
import com.google.android.gms.common.util.Base64Utils
import mx.lixuf.buapp.databinding.AddcardBinding
import mx.lixuf.buapp.model.Card
import mx.lixuf.buapp.util.API
import org.json.JSONObject

class AddCardActivity: AppCompatActivity() {

    private lateinit var binding: AddcardBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = AddcardBinding.inflate(layoutInflater)

        setContentView(binding.root)

        binding.add.setOnClickListener {
            val cardId = intent.extras?.getString(Intent.EXTRA_TEXT) ?: return@setOnClickListener

            val preferences = getSharedPreferences(resources.getString(R.string.app_name), Context.MODE_PRIVATE)
            val accessToken = preferences.getString("accessToken", "null")

            val headers = HashMap<String, Any>()
            headers["Content-Type"] = "application/json"
            headers["Authorization"] = "Bearer $accessToken"

            val parameters = listOf("ID_tarjeta" to cardId)

            Fuel.post(API.getAddCardUrl(), parameters)
                    .header(headers)
                    .responseString { _, _, result ->
                        val (stringResult, error) = result

                        if (stringResult != null) {
                            val json = JSONObject(stringResult)

                            if (json.has("status")) {
                                val resultIntent = Intent()
                                resultIntent.apply {
                                    putExtra(Intent.EXTRA_RETURN_RESULT, json.getString("status"))
                                }

                                setResult(ADD_RESULT_CODE_OK, resultIntent)
                                finish()
                            }
                            else {
                                val resultIntent = Intent()
                                resultIntent.apply {
                                    putExtra(Intent.EXTRA_RETURN_RESULT, "Ocurrió un error al agregar la tarjeta")
                                }

                                setResult(ADD_RESULT_CODE_ERROR, resultIntent)
                                finish()
                            }
                        }
                        else {
                            val resultIntent = Intent()
                            resultIntent.apply {
                                putExtra(Intent.EXTRA_RETURN_RESULT, error?.localizedMessage)
                            }

                            setResult(ADD_RESULT_CODE_ERROR, resultIntent)
                            finish()
                        }
                    }
        }

        binding.cancel.setOnClickListener {
            val resultIntent = Intent()
            resultIntent.apply {
                putExtra(Intent.EXTRA_RETURN_RESULT, "El usuario canceló la operación")
            }

            setResult(ADD_RESULT_CODE_CANCELED, resultIntent)
            finish()
        }

        loadCard()
    }

    private fun loadCard() {
        val cardId = intent.extras?.getString(Intent.EXTRA_TEXT) ?: return

        val preferences = getSharedPreferences(resources.getString(R.string.app_name), Context.MODE_PRIVATE)
        val accessToken = preferences.getString("accessToken", "null")

        val headers = HashMap<String, Any>()
        headers["Content-Type"] = "application/json"
        headers["Authorization"] = "Bearer $accessToken"

        Fuel.get(API.getCardUrl(cardId))
                .header(headers)
                .responseObject<Card> { _, _, result ->
                    val (card, error) = result

                    if (card !=  null) {
                        render(card)
                    }
                    else {
                        val resultIntent = Intent()
                        resultIntent.apply {
                            putExtra(Intent.EXTRA_RETURN_RESULT, error?.localizedMessage)
                        }

                        setResult(ADD_RESULT_CODE_ERROR, resultIntent)
                        finish()
                    }
                }
    }

    private fun render(card: Card) {
        binding.cardView.txtName.text = card.nombre
        binding.cardView.txtProfession.text = card.profession ?: ""

        if (card.whatsapp != null) {
            binding.cardView.btnWhatsapp.setOnClickListener {
                val intent = Intent(Intent.ACTION_SEND).apply {
                    type = "text/plain"
                    putExtra(Intent.EXTRA_PHONE_NUMBER, card.whatsapp)
                    `package` = "com.whatsapp"
                }

                if (intent.resolveActivity(it.context.packageManager) != null) {
                    it.context.startActivity(intent)
                }
            }
        }
        else {
            binding.cardView.btnWhatsapp.visibility = View.GONE
        }

        binding.cardView.btnMail.setOnClickListener {
            val addresses = arrayListOf(card.email)
            val intent = Intent(Intent.ACTION_SENDTO).apply {
                data = Uri.parse("mailto:")
                putExtra(Intent.EXTRA_EMAIL, addresses.toTypedArray())
            }

            if (intent.resolveActivity(it.context.packageManager) != null) {
                it.context.startActivity(intent)
            }
        }

        if (card.phone != null) {
            binding.cardView.btnPhoneCall.setOnClickListener {
                val intent = Intent(Intent.ACTION_DIAL).apply {
                    data = Uri.parse("tel:${card.phone}")
                }

                if (intent.resolveActivity(it.context.packageManager) != null) {
                    it.context.startActivity(intent)
                }
            }
        }
        else {
            binding.cardView.btnPhoneCall.visibility = View.GONE
        }

        binding.cardView.btnShare.setOnClickListener {
            val intent = Intent(Intent.ACTION_SEND).apply {
                putExtra(Intent.EXTRA_TEXT, "https://solyfel.com/share/${card.id}")
                type = "text/*"
            }

            it.context.startActivity(Intent.createChooser(intent, null))
        }

        if (card.direccion != null) {
            binding.cardView.btnMap.setOnClickListener {
                val intent = Intent(Intent.ACTION_VIEW).apply {
                    data = Uri.parse("geo:0,0?q=${card.direccion}")
                }

                if (intent.resolveActivity(it.context.packageManager) != null) {
                    it.context.startActivity(intent)
                }
            }
        }
        else {
            binding.cardView.btnMap.visibility = View.GONE
        }

        val imgArray = Base64Utils.decode(card.image ?: return)

        Glide.with(this)
                .load(imgArray)
                .centerCrop()
                .placeholder(R.drawable.image2)
                .into(binding.cardView.profileImage)
    }

    companion object {
        const val ADD_RESULT_CODE_OK = 1000
        const val ADD_RESULT_CODE_CANCELED = 905
        const val ADD_RESULT_CODE_ERROR = 900
    }
}